import os
import pandas as pd
import nltk
import numpy
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
nltk.download('stopwords')

X = []
Y = []

def readfile(filepath):
    file = open(filepath)
    return file.read()

def getTrainingFiles():
    training_files = []
    files_name = os.listdir('./supervised_learning/3')
    for current_file in files_name:
        message = readfile('./supervised_learning/3' + '/' + current_file)
        training_files.append(message)
    return training_files

def readfilesfromdirectory(path, classification):
    files_name = os.listdir(path)
    for current_file in files_name:
        message = readfile(path + '/' + current_file)
        X.append(message)
        Y.append(classification)

def stopwords():
    symbols = ["'", '"', '`', '.', ',', '-', '!', '?', ':', ';', '(', ')', '*', '--', '\\']
    stopwords = nltk.corpus.stopwords.words('french')
    return stopwords + symbols

readfilesfromdirectory('./supervised_learning/1', '1')
readfilesfromdirectory('./supervised_learning/2', '2')

#print(Y)

training_set = pd.DataFrame({'X': X, 'Y': Y})

#print(training_set)

vectorizer = CountVectorizer(stop_words=stopwords())
counts = vectorizer.fit_transform(training_set['X'].values)

classifier = MultinomialNB()
targets = training_set['Y'].values
classifier.fit(counts, targets)

#print(classifier)

examples = getTrainingFiles()
#print(examples)
example_counts = vectorizer.transform(examples)
#print(example_counts)
predictions = classifier.predict(example_counts)
print(sorted(predictions))
print(type(predictions))

(unique, counts) = numpy.unique(predictions, return_counts=True)
frequencies = numpy.asarray((unique, counts)).T
print(frequencies)
#print(predictions.count(2))
