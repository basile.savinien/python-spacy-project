import numpy as np
from sklearn.utils import Bunch
from os import listdir
from sklearn.feature_extraction.text import TfidfVectorizer

def readFile(filePath):
    file = open(filePath)
    return (file.read())

def generateTfIdf():
    corpus = [
        'This is the first document.',
        'This document is the second document.',
        'And this is the third one.',
        'Is this the first document?',
        ]
    vectorizer = TfidfVectorizer()
    X = vectorizer.fit_transform(createListOfTexts())
    print(len(vectorizer.get_feature_names()))
    print(vectorizer.get_stop_words())
    print(X.shape)

def createListOfTexts():
    list = []
    for f in listdir('discours/tous'):
        tmpList = []
        list.append(readFile('discours/tous/' + f)[0:40])
    print(list)
    return (list)

def main():
    bunch = Bunch()
    cpt = 0
    for f in listdir('discours/tous'):
        cpt += 1
        bunch[f] = readFile('discours/tous/' + f)[0:40]
    #generateTfIdf()
    print(bunch)

main()
