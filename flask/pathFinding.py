from typing import List
from sknetwork.path import shortest_path
import pandas as pd
import numpy as np
import os
from scipy.sparse import csr_matrix
import scipy.sparse

def pathDisplay(pathsIds):
    pathDisplay = ""
    for index, pathId in enumerate(pathsIds):
        if not index:
            pathDisplay = trainStationIdToName[pathId]
        else:
            pathDisplay = pathDisplay + " -> " + trainStationIdToName[pathId]
    return pathDisplay

class Trip:
    def __init__(self, city1, city2, totalDuration, pathsIds):
        self.city1 = city1
        self.city2 = city2
        self.totalDuration = int(totalDuration) if totalDuration != None else None
        self.pathsIds = pathsIds

    def __str__(self):
        return f"Trip from {trainStationIdToName[self.city1]} to {trainStationIdToName[self.city2]} for a duration of {self.totalDuration} min: {pathDisplay(self.pathsIds)}"
    
    
timeTableFile = os.path.join(os.getcwd(), 'timetables2.csv')
print("timeTableFile", timeTableFile)
cols = ["TripId", "TrainStation1", "TrainStation2", "Duration"]

df=pd.read_csv(timeTableFile, header=0, dtype=str, names=cols, encoding='latin1', sep=";")

trainStation = pd.unique(df[['TrainStation1', 'TrainStation2']].values.ravel('K'))
trainStationIdToName = dict(enumerate(trainStation.flatten(), 1))
trainStationNameToId = dict((name, id) for id, name in trainStationIdToName.items())

numberOfTrainstations = len(trainStation)
trips = np.zeros((numberOfTrainstations, numberOfTrainstations))

numberOfTrainstations = len(trainStation)
trips = np.zeros((numberOfTrainstations, numberOfTrainstations))

pathCount = 0
for index, row in df.iterrows():
    pathCount += 1
    idxA = np.argwhere(trainStation == row[1])[0]
    idxB = np.argwhere(trainStation == row[2])[0]
    
    indexTupple = (idxA, idxB) if idxA < idxB else (idxB, idxA)
    trips[indexTupple] = int(row[3])

trips = trips + trips.T - np.diag(np.diag(trips))
#tripMatrix = csr_matrix(trips)
tripMatrix = scipy.sparse.load_npz('sparse_matrix.npz')

print(f"The matrix contains {tripMatrix.getnnz()} trips")

def getCityCorrespondance(city1, city2, trainStationName):
    if city1 in trainStationName:
        return 0
    elif city2 in trainStationName:
        return 1
    else: 
        return None

def getPathIdBetweenCities(city1, city2):
    city1Ids = np.array([])
    city2Ids = np.array([])
    
    # Get all stations that contains the searched name
    for key, value in trainStationNameToId.items():
        index = getCityCorrespondance(city1, city2, key)
        if index != None :
            if index == 0 :
                city1Ids = np.append(city1Ids, value)
            else:
                city2Ids = np.append(city2Ids, value)
    
    if len(city1Ids) > 0 and len(city2Ids) > 0:
        return getTripPathIds(city1Ids, city2Ids)
    else:
        return np.array([])

def getTripPathIds(city1Ids, city2Ids):
    pathIds = []
    similarityInList = set(city1Ids) & set(city2Ids)
    if len(similarityInList) > 0:
        similarity = similarityInList.pop()
        return similarity, similarity
    
    if(len(city1Ids) > 1 and len(city2Ids) > 1):
        for city2Id in city2Ids:
            shortestPathIdIds = shortest_path(tripMatrix, [int(i) for i in city1Ids], [int(city2Id)], 'D')
            for pathId in shortestPathIdIds:
                if isinstance(pathId, list) and len(pathId) >= 2:
                    pathIds.append(pathId)
        return pathIds
    else:
        shortestPathIdIds = shortest_path(tripMatrix, [int(i) for i in city1Ids], [int(i) for i in city2Ids], 'D')
        for pathId in shortestPathIdIds:
            if isinstance(pathId, list) and len(pathId) >= 2:
                pathIds.append(pathId)
        return pathIds
    
    
    
def getBestPathIdFortrip(cityRequested):
    tripArray = np.array(np.zeros(len(cityRequested)-1), dtype=object)
    
    for trip in range(len(tripArray)):
        tripArray[trip] = getMinimalDistance(cityRequested, trip)
        
    return tripArray
        
        
def getMinimalDistance(cityRequested, trip):
    pathIds = getPathIdBetweenCities(cityRequested[trip], cityRequested[trip+1])
    if (len(pathIds) > 0):
        for pathId in pathIds:
            distance = 0
            distanceMin = None
            startId = None
            
            # Iterate though the returned array
            for pathId in pathIds:
                distance = 0
                # Nested array => multiple start/end possible
                if isinstance(pathId, list):
                    for i in range(len(pathId)-1):
                        distance = distance + tripMatrix[(pathId[i], pathId[i+1])]
                    if distanceMin is None or distance < distanceMin:
                        distanceMin = distance
                        pathId = pathId
                        startId = pathId[0]
                        endId = pathId[len(pathId)-1]
                
                # Scalar value => only one pathId possible
                else:
                    for i in range(len(pathIds)-1):
                        distance = distance + tripMatrix[(pathIds[i], pathIds[i+1])]
                    distanceMin = distance
                    pathId = pathIds
                    startId = pathIds[0]
                    endId = pathIds[len(pathIds)-1]
    
        return Trip(startId, endId, distanceMin, pathId)
    else:
        return Trip(None, None, None, None)

def displayPathFindingResult(citysRequested):
    bestTrips = getBestPathIdFortrip(citysRequested)
    for index, bestTrip in enumerate(bestTrips):
        if bestTrip.pathsIds != None:
            print(f"{bestTrip}")
        else:
            print(f"Error for retrieving a trip between {bestTrip.city1} and {bestTrip.city2}")

def generateApiResult(citysRequested):
    bestTrips = getBestPathIdFortrip(citysRequested)
    returningArray = []
    for index, bestTrip in enumerate(bestTrips):
        tmpArray = []
        tmpArray.append(trainStationIdToName[bestTrip.city1])
        tmpArray.append(trainStationIdToName[bestTrip.city2])
        tmpArray.append(bestTrip.totalDuration)
        returningArray.append(tmpArray)
    return returningArray