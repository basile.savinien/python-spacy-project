from urllib import request
import spacy
from algoBis import requestParsingBis

def requestParsing(request):
    nlp = spacy.load("trainedModel")
    doc = nlp(request)
    locations = [[],[],[]]
    idx = 0
    for i in doc.ents:
        print(i.label_, i.text)
        if (i.label_ == 'START'):
            locations[0].append(findNextLocation(doc.ents, idx))
        if (i.label_ == 'STEP'):
            locations[1].append(findNextLocation(doc.ents, idx))
        if (i.label_ == 'END'):
            locations[2].append(findNextLocation(doc.ents, idx))
        idx += 1

    if (len(locations[0]) > 1 or len(locations[0]) == 0) or len(locations[1]) > 1 or len(locations[2]) > 1 or len(cleanArray(locations)) == 1:
        return (requestParsingBis(request))
    else:
        return (cleanArray(locations))
        


def findNextLocation(ents, idx):
    tmpIdx = idx
    while ents[tmpIdx].label_ != 'LOC':
        tmpIdx += 1
    return (ents[tmpIdx].text)

def cleanArray(locations):
    cleanLocationsArray = []
    for location in locations:
        if location:
            cleanLocationsArray.append(location[0])
    return (cleanLocationsArray)
