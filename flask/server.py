from flask import Flask, request, jsonify, make_response
from flask_cors import CORS
from nlp import requestParsing
from pathFinding import generateApiResult

app = Flask(__name__)
CORS(app)

@app.route("/getCitiesOrder", methods=['GET'])
def getCitiesOrder():
    data = request.json
    return jsonify(requestParsing(data['request']))

@app.route("/getPath", methods=['POST'])
def getPath():
    data = request.json
    print(request.json)
    return jsonify(generateApiResult(requestParsing(data['request'])))


if __name__ == "__main__":
    app.run(debug=True, port=80)