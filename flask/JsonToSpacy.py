import json

def convertJson(path):
    f = open(path)
    
    data = json.load(f)
    training_data = []
    for i in data['annotations']:
        entities = []
        for entity in i[1]['entities']:
            entityArray = []
            for element in entity:
                entityArray.append(element)
            entities.append(tuple(entityArray))
        training_data.append((i[0], {"entities" : entities}))
    f.close()
    return(training_data)