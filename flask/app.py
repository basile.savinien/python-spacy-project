import spacy
import numpy as np
import sys

dest_verbs = ["aller", "voyager", "visiter"]

request_verbs = ["veux", "aimerais", "souhaite", "souhaiterais"]

start_words = ["de", "du", "des", "depuis"]

reverse_direction = ['depuis', 'à partir de', 'en provenance de', 'en partant de']

finalDest = ['visiter', 'découvrir', 'aller à']

beginDest = ['en partant de', 'depuis']

stepDest = ['en passant par', 'après être allé à']
        
def requestParsing(request):
    nlp = spacy.load("fr_core_news_lg")
    doc = nlp(request)
    locations = []
    for i in doc.ents:
        if i.label_ == 'LOC' or i.label_ == 'GPE':
                locations.append(i.text)
    if len(locations) <= 1:
        print ("Bad request not enought locations")
    elif len(locations) == 2:
        return findStartEndDest(doc, locations)
    else:
        return findLocationsOrder(doc)
        
def findStartEndDest(doc, locations):
    for word in reverse_direction:
        if str(doc).find(word) != -1:
            locations.reverse()
    return (locations)

def findLocationsOrder(doc):
    returnLocations = []
    for part in beginDest:
        tmpIndex = str(doc).find(part)
        if tmpIndex != -1:
            returnLocations.append(findNextLocationInSentence(str(doc)[tmpIndex + len(part) + 1:]))
            
    for part in stepDest:
        tmpIndex = str(doc).find(part)
        if tmpIndex != -1:
            returnLocations.append(findNextLocationInSentence(str(doc)[tmpIndex + len(part) + 1:]))
            
    for part in finalDest:
        tmpIndex = str(doc).find(part)
        if tmpIndex != -1:
            returnLocations.append(findNextLocationInSentence(str(doc)[tmpIndex + len(part) + 1:]))
            
    return (returnLocations)

def findNextLocationInSentence(endOfString):
    nlp = spacy.load("fr_core_news_lg")
    doc = nlp(endOfString)
    for ent in doc.ents:
        return(str(ent))