import spacy
import random
from spacy.util import minibatch, compounding
from spacy.training.example import Example
from JsonToSpacy import convertJson
from spacy.scorer import Scorer
import pprint

TRAIN_DATA = convertJson('trainData.json')
TEST_DATA = convertJson('testData.json')

def update_model(model='fr_core_news_lg', output_dir='saveModel', n_iter=300):
    if model is not None:
        nlp = spacy.load(model)
        print("Loaded model '%s'" % model)
    else:
        nlp = spacy.blank("fr") 
        print("Created blank 'en' model")

    if "ner" not in nlp.pipe_names:
        ner = nlp.create_pipe("ner")
        nlp.add_pipe(ner, last=True)
    else:
        ner = nlp.get_pipe("ner")

    for _, annotations in TRAIN_DATA:
        for ent in annotations.get("entities"):
            ner.add_label(ent[2])

    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != "ner"]
    with nlp.disable_pipes(*other_pipes):
        if model is None:
            nlp.begin_training()
        for itn in range(n_iter):
            random.shuffle(TRAIN_DATA)
            losses = {}
            batches = minibatch(TRAIN_DATA, size=compounding(4.0, 32.0, 1.001))
            for batch in batches:
                examples = []
                for text, annots in batch:
                    examples.append(Example.from_dict(nlp.make_doc(text), annots))
                nlp.update(examples)
    nlp.to_disk("trainedModel")

def calculate_metrics(model="trainedModel"):
    nlp = spacy.load("trainedModel")

    random.shuffle(TEST_DATA)
    losses = {}
    batches = minibatch(TRAIN_DATA, size=compounding(4.0, 32.0, 1.001))
    for batch in batches:
        examples = []
        for text, annots in batch:
            example = Example.from_dict(nlp.make_doc(text), annots)
            example.predicted = nlp(example.predicted)
            examples.append(example)

    scorer = Scorer(nlp)
    pprint.pprint(scorer.score(examples))
    
#update_model()
calculate_metrics()