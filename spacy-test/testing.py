import unittest
from app import requestParsing

class TestStringMethods(unittest.TestCase):

    def test_1(self):
        self.assertEqual(requestParsing('Je veux aller de Mulhouse à Strasbourg'), ['Mulhouse', 'Strasbourg'])
        
    def test_2(self):
        self.assertEqual(requestParsing('Je veux aller à Marseille à partir de Lyon'), ['Lyon', 'Marseille'])
    
    def test_3(self):
        self.assertEqual(requestParsing('Je veux prendre le train à Mulhouse à destination de Strasbourg'), ['Mulhouse', 'Strasbourg'])
        
    def test_4(self):
        self.assertEqual(requestParsing('Strasbourg en provenance de Mulhouse'), ['Mulhouse', 'Strasbourg'])
        
    def test_5(self):
        self.assertEqual(requestParsing('Je suis à Paris et je veux aller à Strasbourg'), ['Paris', 'Strasbourg'])
        
    def test_6(self):
        self.assertEqual(requestParsing('Je veux aller à Paris après être allé à Bordeaux depuis Lyon'), ['Lyon', 'Bordeaux', 'Paris'])
        
    #def test_7(self):
    #    self.assertEqual(requestParsing("Je souhaiterais faire Paris Gare De l'est Marseille"), ['Paris', 'Marseille'])
        
    def test_8(self):
        self.assertEqual(requestParsing("Lille Amiens"), ['Lille', 'Amiens'])
        
    def test_9(self):
        self.assertEqual(requestParsing("Je veux visiter Paris en partant de Bordeaux et en passant par Nantes"), ['Bordeaux', 'Nantes', 'Paris'])
    
    #def test_7(self):
    #    self.assertEqual(requestParsing("Je veux faire Paris Gare De l'est Marseille"), ["Paris Gare De l'est", 'Marseille'])    
    

if __name__ == '__main__':
    unittest.main()