import spacy
from enum import Enum
from spacy.symbols import PROPN, NOUN, CCONJ, ADP, VERB
import numpy as np
import sys

requests = [
        ("Je veux partir de Mulhouse et visiter Paris depuis Strasbourg"),
        ("J'aimerais aller d'Orléans à Paris puis dans les Vosges"),
        ("Je veux aller à Marseille à partir de Lyon"),
        ("Je veux visiter Paris en partant de Bordeaux et en passant par Nantes"),
        ("Je veux prendre le train à Mulhouse à destination de Strasbourg"),
        ("Strasbourg en provenance de Mulhouse"),
        ("Je veux aller de Mulhouse à Strasbourg"),
        ("Je veux faire Paris Gare De l'est Marseille"),
        ("Je souhaiterais faire Paris Gare De l'est Marseille"),
        ("Je veux aller à Paris après être allé à Mulhouse depuis Lyon"),
        ("Paris-Marseille"),
        ("Je suis à Paris et je veux aller à Strasbourg avec mon amis Frank que je récupère à Mulhouse"),
        ("Je veux voyager de Mulhouse pour visiter Paris en passant par Strasbourg"),
        ("Je veux partir de Mulhouse et visiter Paris depuis la destination de Strasbourg"),
        ("Je veux prendre le train de Mulhouse à destination de Colmar et Strasbourg"),
        ("Je souhaite une pizza napolitaine à Rome"),
        ("Je veux aller à Lyon")
    ]

dest_verbs = ["aller", "voyager", "visiter"]

request_verbs = ["veux", "aimerais", "souhaite", "souhaiterais"]

start_words = ["de", "du", "des", "depuis"]

reverse_direction = ['depuis', 'à partir de', 'en provenance de', 'en partant de']

finalDest = ['visiter', 'découvrir', 'aller à']

beginDest = ['en partant de', 'depuis']

stepDest = ['en passant par', 'après être allé à']

def runNLP():
    for request in requests:
        requestParsing(request)
        
def requestParsing(request):
    nlp = spacy.load("fr_core_news_lg")
    doc = nlp(request)
    locations = []
    for i in doc.ents:
        if i.label_ == 'LOC' or i.label_ == 'GPE':
                locations.append(i.text)
    if len(locations) <= 1:
        print ("Bad request not enought locations")
    elif len(locations) == 2:
        return findStartEndDest(doc, locations)
    else:
        return findLocationsOrder(doc)
        
def findStartEndDest(doc, locations):
    for word in reverse_direction:
        if str(doc).find(word) != -1:
            locations.reverse()
    return (locations)

def findLocationsOrder(doc):
    returnLocations = []
    for part in beginDest:
        tmpIndex = str(doc).find(part)
        if tmpIndex != -1:
            returnLocations.append(findNextLocationInSentence(str(doc)[tmpIndex + len(part) + 1:]))
            
    for part in stepDest:
        tmpIndex = str(doc).find(part)
        if tmpIndex != -1:
            returnLocations.append(findNextLocationInSentence(str(doc)[tmpIndex + len(part) + 1:]))
            
    for part in finalDest:
        tmpIndex = str(doc).find(part)
        if tmpIndex != -1:
            returnLocations.append(findNextLocationInSentence(str(doc)[tmpIndex + len(part) + 1:]))
            
    return (returnLocations)

def findNextLocationInSentence(endOfString):
    nlp = spacy.load("fr_core_news_lg")
    doc = nlp(endOfString)
    for ent in doc.ents:
        return(str(ent))

#runNLP()
#print(requestParsing(sys.argv[1]))
#print(findNextLocationInSentence(sys.argv[1]))