## launch server

pip install virtualenv

virtualenv server

cp flask/nlp.py server/ && cp flask/server.py server/ && cp flask/pathFinding.py server/ && cp flask/timetables2.csv server/ && cp flask/trainModel.py server/ && cp flask/bigData.json server/ && cp flask/JsonToSpacy.py server/ && cp flask/algoBis.py server/

cd server && source bin/activate

pip install spacy

pip install flask flask-cors

pip install scikit-network

pip install pandas

python3 -m spacy download fr_core_news_lg

python3 trainModel.py

python3 server.py

## exit virtual env

deactivate
