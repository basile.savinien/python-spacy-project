import spacy
import numpy as np
import sys

dest_verbs = ["aller", "voyager", "visiter"]

request_verbs = ["veux", "aimerais", "souhaite", "souhaiterais"]

start_words = ["de", "du", "des", "depuis"]

reverse_direction = ['depuis', 'à partir de', 'en provenance de', 'en partant de']

finalDest = ['visiter', 'découvrir', 'aller à']

beginDest = ['en partant de', 'depuis']

stepDest = ['en passant par', 'après être allé à']
        
def requestParsing(request):
    nlp = spacy.load("../spacy-test/trainedModel")
    doc = nlp(request)
    locations = [[],[],[]]
    idx = 0
    for i in doc.ents:
        if (i.label_ == 'START'):
            locations[0].append(findNextLocation(doc.ents, idx))
        if (i.label_ == 'STEP'):
            locations[1].append(findNextLocation(doc.ents, idx))
        if (i.label_ == 'END'):
            locations[2].append(findNextLocation(doc.ents, idx))
        idx += 1
    return (cleanArray(locations))

def findNextLocation(ents, idx):
    tmpIdx = idx
    while ents[tmpIdx].label_ != 'LOC':
        tmpIdx += 1
    return (ents[tmpIdx].text)

def cleanArray(locations):
    cleanLocationsArray = []
    for location in locations:
        if location:
            cleanLocationsArray.append(location[0])
    return (cleanLocationsArray)




    '''if len(locations) <= 1:
        print ("Bad request not enought locations")
    elif len(locations) == 2:
        return findStartEndDest(doc, locations)
    else:
        return findLocationsOrder(doc)'''
        
def findStartEndDest(doc, locations):
    for word in reverse_direction:
        if str(doc).find(word) != -1:
            locations.reverse()
    return (locations)

def findLocationsOrder(doc):
    returnLocations = []
    for part in beginDest:
        tmpIndex = str(doc).find(part)
        if tmpIndex != -1:
            returnLocations.append(findNextLocationInSentence(str(doc)[tmpIndex + len(part) + 1:]))
            
    for part in stepDest:
        tmpIndex = str(doc).find(part)
        if tmpIndex != -1:
            returnLocations.append(findNextLocationInSentence(str(doc)[tmpIndex + len(part) + 1:]))
            
    for part in finalDest:
        tmpIndex = str(doc).find(part)
        if tmpIndex != -1:
            returnLocations.append(findNextLocationInSentence(str(doc)[tmpIndex + len(part) + 1:]))
            
    return (returnLocations)

def findNextLocationInSentence(endOfString):
    nlp = spacy.load("fr_core_news_lg")
    doc = nlp(endOfString)
    for ent in doc.ents:
        return(str(ent))
    
    
print(requestParsing(sys.argv[1]))