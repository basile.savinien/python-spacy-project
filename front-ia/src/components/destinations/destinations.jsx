import styles from "./destinations.module.css"
import arrow from '../../assets/fast-forward.png';
import timePng from '../../assets/time.png';

function Destinations({destinations}) {

    return (
        <div className={styles.container}>
            {destinations.map(([start,end,time]) => {
                return(
                    <div key={`${start}-${end}`} className={styles.row}>
                        <div className={styles.row}>
                            <span>{start} </span>
                            <img src={arrow} className={styles.arrow}  />
                            <span>{end}</span>
                        </div>
                        <div style={{width: 180}} className={styles.row}>
                            <img src={timePng} className={styles.time}  />
                            <span>{Math.floor(time / 60) || ""} {Math.floor(time / 60) > 0 && 'h'} {Math.floor(time % 60 )} min</span>
                        </div>
                    </div>
                )
            })}
        </div>
    );
}

export default Destinations;
