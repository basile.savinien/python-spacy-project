import './homepage.css';
import React, {useEffect, useState} from "react";
import MicIcon from '@mui/icons-material/Mic';
import SpeechRecognition, { useSpeechRecognition } from 'react-speech-recognition'
import axios from "axios";
import Destinations from "../components/destinations/destinations";
import ReactLoading from 'react-loading';

const ending = [
    'merci',
    'fin',
    'à vous',
]

const Homepage = () => {
    const [buttonState, setButtonState] = useState(false);
    const [dest, setDest] = useState(localStorage.getItem('dest') && JSON.parse(localStorage.getItem('dest'))  || []);
    const [loading, setLoading] = useState(false);

    const {
        transcript,
        listening,
        resetTranscript
    } = useSpeechRecognition();

    useEffect(() =>{
        if (ending.some((word) => transcript.includes(word)) ) {
            setButtonState(false)
            setLoading(false)
            SpeechRecognition.stopListening()
        }
    },[transcript])

    const getButtonStyle = () => {
        return (buttonState ? "startRecordingButton enableRecording" : "startRecordingButton disableRecording")
    }

    const speechStart = () => {
        if (buttonState ) {
            setButtonState(false)
            SpeechRecognition.stopListening()
        } else {
            setButtonState(true)
            resetTranscript();
            SpeechRecognition.startListening({ continuous: true })
        }
    }

    const sendQuery = () => {
        setLoading(true)
        if (transcript) {
            axios.post('http://127.0.0.1:80/getPath',{
                "request": transcript.replace(new RegExp(`\\b(${ending.join('|')})\\b`, 'gi'), '')
            }).then((res) => {
                const local = localStorage.getItem('dest') && JSON.parse(localStorage.getItem('dest'))  || [];
                local.unshift(res.data);
                localStorage.setItem('dest', JSON.stringify(local))
                setDest(local);
                setLoading(false)

            })
        }
    }

    return (
        <div>
            <div className={"title"} style={{textAlign: 'center'}} >TRAVEL ORDER RESOLVER</div>
            <div className={"subContainer"}>
                <div className={getButtonStyle()} onClick={speechStart}>
                    {buttonState ? "Terminer l'enregistrement" : "Commencer l'enregistrement"}
                    </div>
                {buttonState && <div className="micIconContainer">
                    <MicIcon fontSize="inherit"/>
                </div>}
                <div className={'text-going'}>{transcript}</div>
                <div className={"button-container"}>
                {!buttonState && !loading && !!transcript.length && <div className={"iWantToGo"} onClick={sendQuery}>Je veux partir !</div>}
                {!buttonState && loading && !!transcript.length && <ReactLoading type={"spin"} color={"#fff"} height={50} width={50} />}
                </div>
            </div>
            <div className={"subContainer"}>
                <div className={"title"}>HISTORIQUE</div>
                <div className={"ctnDest"}>
                {dest.map((d) => {
                    return(
                        <Destinations destinations={d} />
                    )
                })}
                </div>
            </div>
        </div>
    );
}

export default Homepage;
